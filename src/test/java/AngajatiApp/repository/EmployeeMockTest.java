package AngajatiApp.repository;

import AngajatiApp.controller.DidacticFunction;
import AngajatiApp.model.Employee;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

class EmployeeMockTest {
    private List<Employee> employees = new ArrayList<>();
    private Integer numberOfEmployees;
    EmployeeMock employeeMock = new EmployeeMock();

    @BeforeEach
    void setUp() {
        numberOfEmployees = employeeMock.getEmployeeList().size();
        System.out.println("Set up");
    }

    @AfterEach
    void TearDown() {
        System.out.println("Tear Down");
    }

    @Test
    void addValidEmployee() {
        Employee validEmployee = new Employee("Marina", "Moldovan", "1950505125111", DidacticFunction.LECTURER, 2000.00);
        validEmployee.setIdEmployee(101);
        Assertions.assertTrue(employeeMock.addEmployee(validEmployee));
        Assertions.assertEquals((numberOfEmployees + 1), employeeMock.getEmployeeList().size());
    }

    @Test
    void addEmployeeWithNegativeSalary() {
        Employee employeeNegativeSalary = new Employee("Sorana", "Matei", "1940414124567", DidacticFunction.CONFERENTIAR, -1.00);
        employeeNegativeSalary.setIdEmployee(102);
        Assertions.assertFalse(employeeMock.addEmployee(employeeNegativeSalary));
        Assertions.assertEquals((numberOfEmployees), employeeMock.getEmployeeList().size(), "The employee with negative salary was added!");
    }

    @Test
    void addEmployeeWithShortCnp() {
        Employee employeeShortCnp = new Employee("Maria", "Kiss", "1910202125999", DidacticFunction.TEACHER, 2000.00);
        employeeShortCnp.setIdEmployee(103);
        employeeShortCnp.setCnp("1910202");
        Assertions.assertFalse(employeeMock.addEmployee(employeeShortCnp));
        Assertions.assertEquals(numberOfEmployees, employeeMock.getEmployeeList().size(), "The employee with short CNP was added!");

    }

    @Test
    void addEmployeeWithCnp14() {
        Employee employeeCnp14 = new Employee("Andrei", "Dobre", "1901010123123", DidacticFunction.TEACHER, 2000.00);
        employeeCnp14.setIdEmployee(104);
        employeeCnp14.setCnp("19010101231239");
        Assertions.assertFalse(employeeMock.addEmployee(employeeCnp14));
        Assertions.assertEquals(numberOfEmployees, employeeMock.getEmployeeList().size(), "The employee with CNP length 14 was added!");
    }

    @Test
    void addEmployeeWithCnpNull() {
        Employee employeeCnpNull = new Employee();
        employeeCnpNull.setLastName("Dragu");
        employeeCnpNull.setFirstName("Marcela");
        employeeCnpNull.setCnp(null);
        employeeCnpNull.setFunction(DidacticFunction.LECTURER);
        employeeCnpNull.setIdEmployee(105);
        try {
            employeeCnpNull.setSalary(3000.00);
        } catch (Exception salaryExp) {
            assert false;
        }
        Assertions.assertFalse(employeeMock.addEmployee(employeeCnpNull));
        Assertions.assertEquals(numberOfEmployees, employeeMock.getEmployeeList().size(), "The employee with CNP null was added!");
    }

    @Test
    void addEmployeeSalaryEqualsZero() {
        Employee employeeSalaryZero = new Employee("Mona", "Stan", "1930303123122", DidacticFunction.ASISTENT, 0.00);
        employeeSalaryZero.setIdEmployee(106);
        Assertions.assertFalse(employeeMock.addEmployee(employeeSalaryZero));
        Assertions.assertEquals(numberOfEmployees, employeeMock.getEmployeeList().size(), "The employee with salary 0 was added!");

    }

    @Test
    void addEmployeeSalaryEqualsOne() {
        Employee employeeSalaryOne = new Employee("Darius", "Toma", "1901010123432", DidacticFunction.ASISTENT, 0.00);
        employeeSalaryOne.setIdEmployee(107);
        try {
            employeeSalaryOne.setSalary(1.00);
        } catch (Exception e) {
            assert false;
        }
        Assertions.assertTrue(employeeMock.addEmployee(employeeSalaryOne));
        Assertions.assertEquals((numberOfEmployees + 1), employeeMock.getEmployeeList().size(), "The employee with salary 1 was not added!");
    }


    @Test
    void addEmployeeCnp12() {
        Employee employeeCnp12 = new Employee("Faur", "Denisa", "1901010123123", DidacticFunction.ASISTENT, 1200.00);
        employeeCnp12.setIdEmployee(108);
        employeeCnp12.setCnp("190101012399");
        Assertions.assertFalse(employeeMock.addEmployee(employeeCnp12));
        Assertions.assertEquals((numberOfEmployees), employeeMock.getEmployeeList().size(), "The employee with CNP length 12 was added!");
    }

    @Test
    void modifyEmployeeFunction() {
        DidacticFunction newFunction = DidacticFunction.CONFERENTIAR;
        //Employee to modify
        Employee empTest = new Employee("Larisa", "Mihut", "2920502123111", DidacticFunction.ASISTENT, 2000.00);
        //Employee unmodified
        Employee empToCompare = new Employee("Anda", "Moldovan", "2901021123432", DidacticFunction.TEACHER, 3400.00);

        DidacticFunction pastFunction = empTest.getFunction();
        DidacticFunction comparingFunction = empToCompare.getFunction();
        employeeMock.getEmployeeList().add(empToCompare);
        employeeMock.getEmployeeList().add(empTest);

        //If the past didactic function is ok
        Assertions.assertEquals(pastFunction, empTest.getFunction());
        //Modify function
        employeeMock.modifyEmployeeFunction(empTest, newFunction);
        //Verifying if the function was modified
        Assertions.assertEquals(newFunction, empTest.getFunction());
        //Verifying if other member's functions are unmodified
        Assertions.assertEquals(comparingFunction, empToCompare.getFunction());

        //Verifying the method with an employee that is not added to the list
//        Employee empNodAdded = new Employee("Andrei", "Coma", "1900123123432", DidacticFunction.LECTURER, 1900.00);
//        employeeMock.modifyEmployeeFunction(empNodAdded, newFunction);
//        Assertions.assertNotEquals(newFunction, empNodAdded.getFunction());

    }

    @Test
    void modifyEmployeeFunctionNull() {
        Employee emp = null;
        employeeMock.modifyEmployeeFunction(emp, DidacticFunction.LECTURER);
        Assertions.assertNull(emp);
    }
}