package AngajatiApp.model;
import AngajatiApp.controller.DidacticFunction;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;


import java.time.Duration;
import java.util.concurrent.TimeUnit;

import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class EmployeeTest {

    private Employee employee;

    @BeforeEach
    public void setUp() throws Exception {
        employee = new Employee();
        employee.setFirstName("Man");
        employee.setLastName("Flori");
        employee.setCnp("2930701124543");
        employee.setFunction(DidacticFunction.TEACHER);
        employee.setSalary(2500.00);
    }

    @AfterEach
    public void tearDown() {
        System.out.println("Tear down");
    }

    @Test
    public void test_Constructor() {
        Employee plain_employee = new Employee();
        assert(plain_employee.getFirstName().equals(""));
        System.out.println("Plain Constructor is ok");
    }

    @Test
    public void test_ConstructorFields() {
        Employee employee_fields = new Employee("Dan", "Marius", "1890201123653", DidacticFunction.LECTURER, 2900.00);
        assertEquals("Dan", employee_fields.getFirstName());
        System.out.println("Fields Constructor is ok");
    }

    @Test
    public void test_getLastName() {
        assertEquals("Flori", employee.getLastName(), "Last name is not set");
        System.out.println("GetLastName is ok");
    }

    @Test
    public void test_setLastName() {
        employee.setLastName("Florentina");
        assertEquals( "Florentina", employee.getLastName(), "Last name did not change");
        assertFalse(employee.getLastName().equals("Flori"));
        System.out.println("SetLastName is ok");
    }

    @Test
    @Order(2)
    public void test_getFunction() {
        assertEquals(DidacticFunction.TEACHER, employee.getFunction());
        System.out.println("GetFunction is ok");
    }

    @Test
    @Order(1)
    public void test_setSalary() throws Exception {
        employee.setSalary(3000.00);
        assertEquals(3000.00, employee.getSalary());
        System.out.println("Order is ok");
    }

    @ParameterizedTest
    @ValueSource(strings={"Marcel", "Fabian", "Sergiu"})
    public void param_test(String lastName) {
        employee.setLastName(lastName);
        assertEquals(lastName, employee.getLastName(), "The last name is not" + lastName);
        System.out.println("Set LastName " + lastName + " is ok");
    }

    @Test
    public void test_setSalary_valid() {
        try {
            employee.setSalary(2300.00);
            assert(true);
        } catch(Exception e) {
            assert(false);
        }
        System.out.println("Set Salary with valid value is ok!");
    }

    @Disabled
    @Test
    public void test_salary_invalid() {
        try {
            employee.setSalary(-23.23);
            assert(false);

        } catch (Exception exception) {
            assert(true);
            assertEquals("The salary must be a positive number", exception.getMessage().toString());
        }
        System.out.println("Set Salary with invalid data is ok");
    }
    @Disabled
    @Test
    @Timeout(2)
    public void infinite_cycle() {
        int x = 3;
        while(x == 3);
    }


    @Test
    public void test_timeout() {
        assertTimeout(Duration.ofMillis(1000), () -> {
            while (true) ;
        });
    }

}